/**
 * 
 */
package udea.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Stephany Berrio
 *
 */
@XmlRootElement
public class RespuestaPeticionDTO {
	private Integer id;// id del registro
	private String descripcion;// respuesta que se da a la peticion
	private Date fechaRespuesta;//fecha de la respuesta
	private PeticionDTO peticionId;//id de la peticion a la cual sele relaciona la respuesta
	private UsuarioDTO usuarioId;// usuario q responde
	
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return the fechaRespuesta
	 */
	public Date getFechaRespuesta() {
		return fechaRespuesta;
	}
	/**
	 * @param fechaRespuesta the fechaRespuesta to set
	 */
	public void setFechaRespuesta(Date fechaRespuesta) {
		this.fechaRespuesta = fechaRespuesta;
	}
	/**
	 * @return the peticionId
	 */
	public PeticionDTO getPeticionId() {
		return peticionId;
	}
	/**
	 * @param peticionId the peticionId to set
	 */
	public void setPeticionId(PeticionDTO peticionId) {
		this.peticionId = peticionId;
	}
	/**
	 * @return the usuarioId
	 */
	public UsuarioDTO getUsuarioId() {
		return usuarioId;
	}
	/**
	 * @param usuarioId the usuarioId to set
	 */
	public void setUsuarioId(UsuarioDTO usuarioId) {
		this.usuarioId = usuarioId;
	}
	
	
}
