/**
 * 
 */
package udea.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Stephany Berrio Alzate
 *
 */
@XmlRootElement
public class UsuarioDTO {
	private Integer id;//id
	private String nombre;// nombre del usuario
	private String email;// email del usuario
	private String usuario;// nombre de usuario del usuario
	private String telefono;// telefono del usuario
	private String contrasena;// contraseña del usuario
	private String tipoUsuario;// tipo de usuario
	/**
	 * @return el id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id 
	 * Actualiza el id con el parametro recibido
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return nombre
	 * 
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre 
	 * Acualiza el nombre con el parametro recibido
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return el email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email 
	 * Actualiza el email con el parametro recibido
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return el usuario
	 */
	public String getUsuario() {
		return usuario;
	}
	/**
	 * @param usuario 
	 * Actualiza el usuario con el parametro recibido
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	/**
	 * @return el telefono
	 */
	public String getTelefono() {
		return telefono;
	}
	/**
	 * @param telefono 
	 * Actualiza el telefono con el parametro recibido
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	/**
	 * @return la contrasena
	 */
	public String getContrasena() {
		return contrasena;
	}
	/**
	 * @param contrasena 
	 * Actualiza la contrasena con el parametro recibido
	 */
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	/**
	 * @return el tipoUsuario
	 */
	public String getTipoUsuario() {
		return tipoUsuario;
	}
	/**
	 * @param tipoUsuario 
	 * Actualiza el tipoUsuario con el parametro recibido
	 */
	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	
}
