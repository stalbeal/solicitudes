/**
 * 
 */
package udea.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author stephys
 *
 */
@XmlRootElement
public class SucursalDTO {
	private Integer id;//id del registro
	private String nombre;// nombre de la sucursal
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
