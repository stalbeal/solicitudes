/**
 * 
 */
package udea.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Stephany Berrio Alzate
 *
 */
@XmlRootElement
public class PeticionDTO {

	private Integer id;// id del registro
	private String nombres;// nombre del cliente
	private String apellidos;// apellidos del cliente
	private String email;// email del cliente
	private String telefono;// telefono del cliente
	private String celular;// celular del cliente
	private String descripcion;// descripcion de la peticion
	private Date fechaSolicitud;// fecha en q se realiza la solicitud
	private boolean estadoPeticion;// estado de la peticion
	private TipoPeticionDTO tipoPeticionId;// tipo de la peticion
	private ProductoDTO productoId;// producto sobre el cual se hace peticion
	private SucursalDTO sucursalId;// sucursal en la cual se compro el producto

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the nombres
	 */
	public String getNombres() {
		return nombres;
	}

	/**
	 * @param nombres
	 *            the nombres to set
	 */
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	/**
	 * @return the apellidos
	 */
	public String getApellidos() {
		return apellidos;
	}

	/**
	 * @param apellidos
	 *            the apellidos to set
	 */
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * @param telefono
	 *            the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * @return the celular
	 */
	public String getCelular() {
		return celular;
	}

	/**
	 * @param celular
	 *            the celular to set
	 */
	public void setCelular(String celular) {
		this.celular = celular;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the fechaSolicitud
	 */
	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}

	/**
	 * @param fechaSolicitud
	 *            the fechaSolicitud to set
	 */
	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	/**
	 * @return the estadoPeticion
	 */
	public boolean isEstadoPeticion() {
		return estadoPeticion;
	}

	/**
	 * @param estadoPeticion
	 *            the estadoPeticion to set
	 */
	public void setEstadoPeticion(boolean estadoPeticion) {
		this.estadoPeticion = estadoPeticion;
	}

	/**
	 * @return the tipoPeticionId
	 */
	public TipoPeticionDTO getTipoPeticionId() {
		return tipoPeticionId;
	}

	/**
	 * @param tipoPeticionId
	 *            the tipoPeticionId to set
	 */
	public void setTipoPeticionId(TipoPeticionDTO tipoPeticionId) {
		this.tipoPeticionId = tipoPeticionId;
	}

	/**
	 * @return the productoId
	 */
	public ProductoDTO getProductoId() {
		return productoId;
	}

	/**
	 * @param productoId
	 *            the productoId to set
	 */
	public void setProductoId(ProductoDTO productoId) {
		this.productoId = productoId;
	}

	/**
	 * @return the sucursalId
	 */
	public SucursalDTO getSucursalId() {
		return sucursalId;
	}

	/**
	 * @param sucursalId
	 *            the sucursalId to set
	 */
	public void setSucursalId(SucursalDTO sucursalId) {
		this.sucursalId = sucursalId;
	}

}
