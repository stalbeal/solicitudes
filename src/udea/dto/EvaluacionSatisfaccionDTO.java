/**
 *  Clase DTO que se relaciona con la tabla de
 *  evaluacin_satisfaccion
 */
package udea.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author stephys
 *
 */
@XmlRootElement
public class EvaluacionSatisfaccionDTO {
	/**
	 * Declaración de Variables
	 */
	private String agrado;
	private String visitaSucursal;
	private Integer id;
	private RespuestaPeticionDTO respuestaPeticion;
	
	
	/**
	 * @return the respuestaPeticion
	 */
	public RespuestaPeticionDTO getRespuestaPeticion() {
		return respuestaPeticion;
	}
	/**
	 * @param respuestaPeticion the respuestaPeticion to set
	 */
	public void setRespuestaPeticion(RespuestaPeticionDTO respuestaPeticion) {
		this.respuestaPeticion = respuestaPeticion;
	}
	/**
	 * @return the agrado
	 */
	public String getAgrado() {
		return agrado;
	}
	/**
	 * @param agrado the agrado to set
	 */
	public void setAgrado(String agrado) {
		this.agrado = agrado;
	}
	/**
	 * @return the visitaSucursal
	 */
	public String getVisitaSucursal() {
		return visitaSucursal;
	}
	/**
	 * @param visitaSucursal the visitaSucursal to set
	 */
	public void setVisitaSucursal(String visitaSucursal) {
		this.visitaSucursal = visitaSucursal;
	}
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	

}
