///**
// * 
// */
//package udea.util;
//
//import java.util.*;
//
//import javax.mail.*;
//import javax.mail.internet.*;
//import javax.activation.*;
//
///**
// * @author Stephany Berrio Alzate
// *
// */
//public class EmailServicio {
//
//	/**
//	 * Servicio para envio de email
//	 * @param username
//	 * @param password
//	 * @param to
//	 * @param subject
//	 * @param content
//	 */
//	public static void sendEmail(final String username, final String password, String to, String subject, String content ) {
//
//        Properties props = new Properties();
//        props.put("mail.smtp.auth", "true");
//        props.put("mail.smtp.starttls.enable", "true");
//        props.put("mail.smtp.host", "smtp.gmail.com");
//        props.put("mail.smtp.port", "587");
// 
//        Session session = Session.getInstance(props,
//                new javax.mail.Authenticator() {
//                    protected PasswordAuthentication getPasswordAuthentication() {
//                        return new PasswordAuthentication(username, password);
//                    }
//                });
// 
//        try {
// 
//            Message message = new MimeMessage(session);
//            message.setFrom(new InternetAddress(username));
//            message.setRecipients(Message.RecipientType.TO,
//                    InternetAddress.parse(to));
//            message.setSubject(subject);
//            message.setText(content);
// 
//            Transport.send(message);
//            System.out.println("Su mensaje ha sido enviado");
// 
//        } catch (MessagingException e) {
//            throw new RuntimeException(e);
//        }
//    
//	}
//}
