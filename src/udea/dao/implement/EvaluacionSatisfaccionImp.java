/**
 * Implementacion de la interfaz IEvaluacionSatisfaccion
 * ver udea.dao.interfaces.IEvaluacionSatisfaccion
 */
package udea.dao.implement;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import udea.dao.interfaces.IEvaluacionSatisfaccion;
import udea.dto.EvaluacionSatisfaccionDTO;
import udea.dto.RespuestaPeticionDTO;
import udea.exception.MyException;

/**
 * @author Stephany Berrio Alzate
 *
 */
public class EvaluacionSatisfaccionImp extends HibernateDaoSupport implements
		IEvaluacionSatisfaccion {
	Logger log = Logger.getLogger(this.getClass());

	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IEvaluacionSatisfaccion#insert(udea.dto.EvaluacionSatisfaccionDTO)
	 */
	@Override
	public void insert(EvaluacionSatisfaccionDTO obj) throws MyException {
		Session session = null;
		try {
			session = getSession();
			RespuestaPeticionDTO res=(RespuestaPeticionDTO)session.get(RespuestaPeticionDTO.class, obj.getRespuestaPeticion().getId());
			if(res==null){
				return;
			}
			Transaction tx = session.beginTransaction();
			session.save(obj);
			tx.commit();
		} catch (HibernateException e) {
			log.error("ocurrio un error guardando", e);
			throw new MyException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IEvaluacionSatisfaccion#update(udea.dto.EvaluacionSatisfaccionDTO)
	 */
	@Override
	public void update(EvaluacionSatisfaccionDTO obj) throws MyException {
		Session session = null;
		try {
			session = getSession();
			Transaction tx = session.beginTransaction();
			session.update(obj);
			tx.commit();
		} catch (HibernateException e) {
			log.error("ocurrio un error actualizando", e);
			throw new MyException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IEvaluacionSatisfaccion#remove(udea.dto.EvaluacionSatisfaccionDTO)
	 */
	@Override
	public void remove(EvaluacionSatisfaccionDTO obj) throws MyException {
		Session session = null;
		try {
			session =getSession();
			Transaction tx = session.beginTransaction();
			session.delete(obj);
			tx.commit();

		} catch (HibernateException e) {
			e.printStackTrace();
			throw new MyException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IEvaluacionSatisfaccion#getAll()
	 */
	@Override
	public List<EvaluacionSatisfaccionDTO> getAll() throws MyException {
		List<EvaluacionSatisfaccionDTO> evaluaciones = null;
		Session session = null;
		try {
			session = getSession();
			Criteria criteria = session
					.createCriteria(EvaluacionSatisfaccionDTO.class);
			evaluaciones = criteria.list();
		} catch (HibernateException e) {
			throw new MyException(e);
		}
		return evaluaciones;
	}

	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IEvaluacionSatisfaccion#get(java.lang.Integer)
	 */
	@Override
	public EvaluacionSatisfaccionDTO get(Integer id) throws MyException {
		EvaluacionSatisfaccionDTO evaluacion = null;
		Session session = null;
		try {
			session = getSession();
			Criteria criteria = session.createCriteria(
					EvaluacionSatisfaccionDTO.class).add(
					Restrictions.eq("id", id));
			evaluacion = (EvaluacionSatisfaccionDTO) criteria.uniqueResult();
		} catch (HibernateException e) {
			throw new MyException(e);
		}
		return evaluacion;
	}

}
