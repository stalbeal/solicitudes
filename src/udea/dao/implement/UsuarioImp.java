/**
 * Implementacion de la interfaz IUsuario
 * ver udea.dao.interfaces.IUsuario
 */
package udea.dao.implement;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import udea.dao.interfaces.IUsuario;
import udea.dto.UsuarioDTO;
import udea.exception.MyException;

/**
 * @author Stephany Berrio Alzate
 *
 */
public class UsuarioImp extends HibernateDaoSupport implements IUsuario{
	/**
	 * Declaracion de logger para llevar un registro de los sucesos 
	 * importantes o de depuración
	 */
	Logger log = Logger.getLogger(this.getClass());
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IUsuario#insert(udea.dto.UsuarioDTO)
	 */
	@Override
	public void insert(UsuarioDTO obj) throws MyException {
		Session session = null;
		try {
			//Se accede a la sesion de la base de datos
			session = getSession();
			//Se inicializa la transaccion
			Transaction tx = session.beginTransaction();
			//inserta en la base de datos
			session.save(obj);
			//confirma la transaccion
			tx.commit();
		} catch (HibernateException e) {
			log.error("ocurrio un error guardando", e);
			throw new MyException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IUsuario#update(udea.dto.UsuarioDTO)
	 */
	@Override
	public void update(UsuarioDTO obj) throws MyException {
		Session session = null;
		try {
			session = getSession();
			Transaction tx = session.beginTransaction();
			session.update(obj);
			tx.commit();
		} catch (HibernateException e) {
			log.error("ocurrio un error actualizando", e);
			throw new MyException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IUsuario#remove(udea.dto.UsuarioDTO)
	 */
	@Override
	public void remove(UsuarioDTO obj) throws MyException {
		Session session = null;
		try {
			session =getSession();
			Transaction tx = session.beginTransaction();
			session.delete(obj);
			tx.commit();

		} catch (HibernateException e) {
			e.printStackTrace();
			throw new MyException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IUsuario#getAll()
	 */
	@Override
	public List<UsuarioDTO> getAll() throws MyException {
		List<UsuarioDTO> usuarios = null;
		Session session = null;
		try {
			session = getSession();
			Criteria criteria = session
					.createCriteria(UsuarioDTO.class);
			usuarios = criteria.list();
		} catch (HibernateException e) {
			throw new MyException(e);
		}
		return usuarios;
	}
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IUsuario#get(java.lang.Integer)
	 */
	@Override
	public UsuarioDTO get(String username) throws MyException {
		UsuarioDTO usuario = null;
		Session session = null;
		try {
			session = getSession();
			Criteria criteria = session.createCriteria(
					UsuarioDTO.class).add(
					Restrictions.eq("usuario", username));
			usuario = (UsuarioDTO) criteria.uniqueResult();
		} catch (HibernateException e) {
			throw new MyException(e);
		}
		return usuario;
	}

}

