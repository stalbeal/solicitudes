/**
 * Implementacion de la interfaz IRespuestaRespuestaPeticion
 * ver udea.dao.interfaces.IRespuestaRespuestaPeticion
 */
package udea.dao.implement;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import udea.dao.interfaces.IRespuestaPeticion;
import udea.dto.PeticionDTO;
import udea.dto.RespuestaPeticionDTO;
import udea.dto.UsuarioDTO;
import udea.exception.MyException;

/**
 * @author Stephany Berrio Alzate
 *
 */
public class RespuestaPeticionImp extends HibernateDaoSupport implements IRespuestaPeticion{
	/**
	 * Declaracion de logger para llevar un registro de los sucesos 
	 * importantes o de depuración
	 */
	Logger log = Logger.getLogger(this.getClass());
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IRespuestaPeticion#insert(udea.dto.RespuestaPeticionDTO)
	 */
	@Override
	public void insert(RespuestaPeticionDTO obj) throws MyException {
		Session session = null;
		try {
			session = getSession();
			UsuarioDTO usuario=(UsuarioDTO)session.get(UsuarioDTO.class, obj.getUsuarioId().getId());
			PeticionDTO peticion=(PeticionDTO)session.get(PeticionDTO.class, obj.getPeticionId().getId());
			if(usuario==null || peticion==null){
				return;
			}
			peticion.setEstadoPeticion(true);
			Transaction tx = session.beginTransaction();
			session.save(obj);
			tx.commit();
		} catch (HibernateException e) {
			log.error("ocurrio un error guardando", e);
			throw new MyException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IRespuestaPeticion#update(udea.dto.RespuestaPeticionDTO)
	 */
	@Override
	public void update(RespuestaPeticionDTO obj) throws MyException {
		Session session = null;
		try {
			session = getSession();
			Transaction tx = session.beginTransaction();
			session.update(obj);
			tx.commit();
		} catch (HibernateException e) {
			log.error("ocurrio un error actualizando", e);
			throw new MyException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IRespuestaPeticion#remove(udea.dto.RespuestaPeticionDTO)
	 */
	@Override
	public void remove(RespuestaPeticionDTO obj) throws MyException {
		Session session = null;
		try {
			session =getSession();
			Transaction tx = session.beginTransaction();
			session.delete(obj);
			tx.commit();

		} catch (HibernateException e) {
			e.printStackTrace();
			throw new MyException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IRespuestaPeticion#getAll()
	 */
	@Override
	public List<RespuestaPeticionDTO> getAll() throws MyException {
		List<RespuestaPeticionDTO> respuestas = null;
		Session session = null;
		try {
			session = getSession();
			Criteria criteria = session
					.createCriteria(RespuestaPeticionDTO.class);
			respuestas = criteria.list();
		} catch (HibernateException e) {
			throw new MyException(e);
		}
		return respuestas;
	}
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IRespuestaPeticion#get(java.lang.Integer)
	 */
	@Override
	public RespuestaPeticionDTO get(Integer id) throws MyException {
		RespuestaPeticionDTO respuesta = null;
		Session session = null;
		try {
			session = getSession();
			Criteria criteria = session.createCriteria(
					RespuestaPeticionDTO.class).add(
					Restrictions.eq("id", id));
			respuesta = (RespuestaPeticionDTO) criteria.uniqueResult();
		} catch (HibernateException e) {
			throw new MyException(e);
		}
		return respuesta;
	}

}

