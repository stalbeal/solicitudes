/**
 * Implementacion de la interfaz IPeticion
 * ver udea.dao.interfaces.IPeticion
 */
package udea.dao.implement;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import udea.dao.interfaces.IPeticion;
import udea.dto.PeticionDTO;
import udea.dto.ProductoDTO;
import udea.dto.SucursalDTO;
import udea.dto.TipoPeticionDTO;
import udea.exception.MyException;

/**
 * @author Stephany Berrio Alzate
 *
 */
public class PeticionImp extends HibernateDaoSupport implements IPeticion{
	/**
	 * Declaracion de logger para llevar un registro de los sucesos 
	 * importantes o de depuración
	 */
	Logger log = Logger.getLogger(this.getClass());
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IPeticion#insert(udea.dto.PeticionDTO)
	 */
	@Override
	public void insert(PeticionDTO obj) throws MyException {
		Session session = null;
		try {
			session = getSession();

			ProductoDTO producto=(ProductoDTO) session.get(ProductoDTO.class, obj.getProductoId().getId());
			SucursalDTO sucursal=(SucursalDTO)session.get(SucursalDTO.class, obj.getSucursalId().getId());
			TipoPeticionDTO tipo=(TipoPeticionDTO)session.get(TipoPeticionDTO.class, obj.getTipoPeticionId().getId());
			if(producto==null || sucursal==null || tipo==null){
				return;
			}
			Transaction tx = session.beginTransaction();
			session.save(obj);
			tx.commit();
		} catch (HibernateException e) {
			log.error("ocurrio un error guardando", e);
			throw new MyException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IPeticion#update(udea.dto.PeticionDTO)
	 */
	@Override
	public void update(PeticionDTO obj) throws MyException {
		Session session = null;
		try {
			session = getSession();
			Transaction tx = session.beginTransaction();
			session.update(obj);
			tx.commit();
		} catch (HibernateException e) {
			log.error("ocurrio un error actualizando", e);
			throw new MyException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IPeticion#remove(udea.dto.PeticionDTO)
	 */
	@Override
	public void remove(PeticionDTO obj) throws MyException {
		Session session = null;
		try {
			session =getSession();
			Transaction tx = session.beginTransaction();
			session.delete(obj);
			tx.commit();

		} catch (HibernateException e) {
			e.printStackTrace();
			throw new MyException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IPeticion#getAll()
	 */
	@Override
	public List<PeticionDTO> getAll() throws MyException {
		List<PeticionDTO> peticiones = null;
		Session session = null;
		try {
			session = getSession();
			Criteria criteria = session
					.createCriteria(PeticionDTO.class);
			peticiones = criteria.list();
		} catch (HibernateException e) {
			throw new MyException(e);
		}
		return peticiones;
	}
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IPeticion#get(java.lang.Integer)
	 */
	@Override
	public PeticionDTO get(Integer id) throws MyException {
		PeticionDTO peticion = null;
		Session session = null;
		try {
			session = getSession();
			Criteria criteria = session.createCriteria(
					PeticionDTO.class).add(
					Restrictions.eq("id", id));
			peticion = (PeticionDTO) criteria.uniqueResult();
		} catch (HibernateException e) {
			throw new MyException(e);
		}
		return peticion;
	}

}

