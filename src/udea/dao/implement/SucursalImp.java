/**
 * Implementacion de la interfaz ISucursal
 * ver udea.dao.interfaces.ISucursal
 */
package udea.dao.implement;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import udea.dao.interfaces.ISucursal;
import udea.dto.SucursalDTO;
import udea.exception.MyException;

/**
 * @author Stephany Berrio Alzate
 *
 */
public class SucursalImp extends HibernateDaoSupport implements ISucursal{
	/**
	 * Declaracion de logger para llevar un registro de los sucesos 
	 * importantes o de depuración
	 */
	Logger log = Logger.getLogger(this.getClass());
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.ISucursal#insert(udea.dto.SucursalDTO)
	 */
	@Override
	public void insert(SucursalDTO obj) throws MyException {
		Session session = null;
		try {
			session = getSession();
			Transaction tx = session.beginTransaction();
			session.save(obj);
			tx.commit();
		} catch (HibernateException e) {
			log.error("ocurrio un error guardando", e);
			throw new MyException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.ISucursal#update(udea.dto.SucursalDTO)
	 */
	@Override
	public void update(SucursalDTO obj) throws MyException {
		Session session = null;
		try {
			session = getSession();
			Transaction tx = session.beginTransaction();
			session.update(obj);
			tx.commit();
		} catch (HibernateException e) {
			log.error("ocurrio un error actualizando", e);
			throw new MyException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.ISucursal#remove(udea.dto.SucursalDTO)
	 */
	@Override
	public void remove(SucursalDTO obj) throws MyException {
		Session session = null;
		try {
			session =getSession();
			Transaction tx = session.beginTransaction();
			session.delete(obj);
			tx.commit();

		} catch (HibernateException e) {
			e.printStackTrace();
			throw new MyException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.ISucursal#getAll()
	 */
	@Override
	public List<SucursalDTO> getAll() throws MyException {
		List<SucursalDTO> sucursales = null;
		Session session = null;
		try {
			session = getSession();
			Criteria criteria = session
					.createCriteria(SucursalDTO.class);
			sucursales = criteria.list();
		} catch (HibernateException e) {
			throw new MyException(e);
		}
		return sucursales;
	}
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.ISucursal#get(java.lang.Integer)
	 */
	@Override
	public SucursalDTO get(Integer id) throws MyException {
		SucursalDTO sucursal = null;
		Session session = null;
		try {
			session = getSession();
			Criteria criteria = session.createCriteria(
					SucursalDTO.class).add(
					Restrictions.eq("id", id));
			sucursal = (SucursalDTO) criteria.uniqueResult();
		} catch (HibernateException e) {
			throw new MyException(e);
		}
		return sucursal;
	}

}

