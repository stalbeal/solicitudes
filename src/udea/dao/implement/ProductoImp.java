/**
 * Iplementacion de la interfaz IProducto
 * ver udea.dao.interfaces.IProducto
 */
package udea.dao.implement;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import udea.dao.interfaces.IProducto;
import udea.dto.ProductoDTO;
import udea.exception.MyException;

/**
 * @author Stephany Berrio Alzate
 *
 */
public class ProductoImp extends HibernateDaoSupport implements IProducto{

	Logger log = Logger.getLogger(this.getClass());

	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IProducto#insert(udea.dto.ProductoDTO)
	 */
	@Override
	public void insert(ProductoDTO obj) throws MyException {
		Session session = null;
		try {
			session = getSession();
			Transaction tx = session.beginTransaction();
			session.save(obj);
			tx.commit();
		} catch (HibernateException e) {
			log.error("ocurrio un error guardando", e);
			throw new MyException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IProducto#update(udea.dto.ProductoDTO)
	 */
	@Override
	public void update(ProductoDTO obj) throws MyException {
		Session session = null;
		try {
			session = getSession();
			Transaction tx = session.beginTransaction();
			session.update(obj);
			tx.commit();
		} catch (HibernateException e) {
			log.error("ocurrio un error actualizando", e);
			throw new MyException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IProducto#remove(udea.dto.ProductoDTO)
	 */
	@Override
	public void remove(ProductoDTO obj) throws MyException {
		Session session = null;
		try {
			session =getSession();
			Transaction tx = session.beginTransaction();
			session.delete(obj);
			tx.commit();

		} catch (HibernateException e) {
			e.printStackTrace();
			throw new MyException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IProducto#getAll()
	 */
	@Override
	public List<ProductoDTO> getAll() throws MyException {
		List<ProductoDTO> productos = null;
		Session session = null;
		try {
			session = getSession();
			Criteria criteria = session
					.createCriteria(ProductoDTO.class);
			productos = criteria.list();
		} catch (HibernateException e) {
			throw new MyException(e);
		}
		return productos;
	}

	/* (non-Javadoc)
	 * @see udea.dao.interfaces.IProducto#get(java.lang.Integer)
	 */
	@Override
	public ProductoDTO get(Integer id) throws MyException {
		ProductoDTO producto = null;
		Session session = null;
		try {
			session = getSession();
			Criteria criteria = session.createCriteria(
					ProductoDTO.class).add(
					Restrictions.eq("id", id));
			producto = (ProductoDTO) criteria.uniqueResult();
		} catch (HibernateException e) {
			throw new MyException(e);
		}
		return producto;
	}

}


