/**
 * Implementacion de la interfaz ITipoPeticion
 * ver udea.dao.interfaces.ITipoPeticion
 */
package udea.dao.implement;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import udea.dao.interfaces.ITipoPeticion;
import udea.dto.TipoPeticionDTO;
import udea.exception.MyException;

/**
 * @author Stephany Berrio Alzate
 *
 */
public class TipoPeticionImp extends HibernateDaoSupport implements ITipoPeticion{
	/**
	 * Declaracion de logger para llevar un registro de los sucesos 
	 * importantes o de depuración
	 */
	Logger log = Logger.getLogger(this.getClass());
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.ITipoPeticion#insert(udea.dto.TipoPeticionDTO)
	 */
	@Override
	public void insert(TipoPeticionDTO obj) throws MyException {
		Session session = null;
		try {
			session = getSession();
			Transaction tx = session.beginTransaction();
			session.save(obj);
			tx.commit();
		} catch (HibernateException e) {
			log.error("ocurrio un error guardando", e);
			throw new MyException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.ITipoPeticion#update(udea.dto.TipoPeticionDTO)
	 */
	@Override
	public void update(TipoPeticionDTO obj) throws MyException {
		Session session = null;
		try {
			session = getSession();
			Transaction tx = session.beginTransaction();
			session.update(obj);
			tx.commit();
		} catch (HibernateException e) {
			log.error("ocurrio un error actualizando", e);
			throw new MyException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.ITipoPeticion#remove(udea.dto.TipoPeticionDTO)
	 */
	@Override
	public void remove(TipoPeticionDTO obj) throws MyException {
		Session session = null;
		try {
			session =getSession();
			Transaction tx = session.beginTransaction();
			session.delete(obj);
			tx.commit();

		} catch (HibernateException e) {
			e.printStackTrace();
			throw new MyException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.ITipoPeticion#getAll()
	 */
	@Override
	public List<TipoPeticionDTO> getAll() throws MyException {
		List<TipoPeticionDTO> tipoPeticiones = null;
		Session session = null;
		try {
			session = getSession();
			Criteria criteria = session
					.createCriteria(TipoPeticionDTO.class);
			tipoPeticiones = criteria.list();
		} catch (HibernateException e) {
			throw new MyException(e);
		}
		return tipoPeticiones;
	}
	
	/* (non-Javadoc)
	 * @see udea.dao.interfaces.ITipoPeticion#get(java.lang.Integer)
	 */
	@Override
	public TipoPeticionDTO get(Integer id) throws MyException {
		TipoPeticionDTO tipoPeticion = null;
		Session session = null;
		try {
			session = getSession();
			Criteria criteria = session.createCriteria(
					TipoPeticionDTO.class).add(
					Restrictions.eq("id", id));
			tipoPeticion = (TipoPeticionDTO) criteria.uniqueResult();
		} catch (HibernateException e) {
			throw new MyException(e);
		}
		return tipoPeticion;
	}

}

