/**
 * Interfaz que define los metodos de la clase
 */
package udea.dao.interfaces;

import java.util.List;
import udea.dto.PeticionDTO;
import udea.exception.MyException;

/**
 * @author Stephany Berrio Alzate
 *
 */
public interface IPeticion {
	/**
	 * @return una lista de objetos de la clase 
	 * @throws MyException
	 */
	public List<PeticionDTO> getAll() throws MyException ;
	/**
	 * @param id
	 * @return Devuelve el objeto de la clase con el id recibido
	 * @throws MyException
	 */
	public PeticionDTO get(Integer id) throws MyException ;
	/**
	 * Inserta en la base de datos el objeto recibido
	 * @param obj 
	 * @throws MyException
	 */
	public void insert(PeticionDTO obj)throws MyException ;
	/**
	 * Actualiza el objeto recibido, en la base de datos
	 * @param obj
	 * @throws MyException
	 */
	public void update(PeticionDTO obj)throws MyException ;
	/**
	 * Elimina el objeto recibido
	 * @param obj
	 * @throws MyException
	 */
	public void remove(PeticionDTO obj)throws MyException ;

}
