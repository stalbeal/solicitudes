/**
 * Interfaz que define los metodos de la clase
 */
package udea.dao.interfaces;

import java.util.List;

import udea.dto.ProductoDTO;
import udea.exception.MyException;

/**
 * @author Stephany Berrio Alzate
 *
 */
public interface IProducto {
	/**
	 * @return una lista de objetos de la clase 
	 * @throws MyException
	 */
	public List<ProductoDTO> getAll() throws MyException ;
	/**
	 * @param id
	 * @return Devuelve el objeto de la clase con el id recibido
	 * @throws MyException
	 */
	public ProductoDTO get(Integer id) throws MyException ;
	/**
	 * Inserta en la base de datos el objeto recibido
	 * @param obj 
	 * @throws MyException
	 */
	public void insert(ProductoDTO obj)throws MyException ;
	/**
	 * Actualiza el objeto recibido, en la base de datos
	 * @param obj
	 * @throws MyException
	 */
	public void update(ProductoDTO obj)throws MyException ;
	/**
	 * Elimina el objeto recibido
	 * @param obj
	 * @throws MyException
	 */
	public void remove(ProductoDTO obj)throws MyException ;

}
