/**
 * Interfaz que define los metodos de la clase
 */
package udea.dao.interfaces;

import java.util.List;

import udea.dto.RespuestaPeticionDTO;
import udea.exception.MyException;

/**
 * @author Stephany Berrio Alzate
 *
 */
public interface IRespuestaPeticion {
	/**
	 * @return una lista de objetos de la clase 
	 * @throws MyException
	 */
	public List<RespuestaPeticionDTO> getAll() throws MyException ;
	/**
	 * @param id
	 * @return Devuelve el objeto de la clase con el id recibido
	 * @throws MyException
	 */
	public RespuestaPeticionDTO get(Integer id) throws MyException ;
	/**
	 * Inserta en la base de datos el objeto recibido
	 * @param obj 
	 * @throws MyException
	 */
	public void insert(RespuestaPeticionDTO obj)throws MyException ;
	/**
	 * Actualiza el objeto recibido, en la base de datos
	 * @param obj
	 * @throws MyException
	 */
	public void update(RespuestaPeticionDTO obj)throws MyException ;
	/**
	 * Elimina el objeto recibido
	 * @param obj
	 * @throws MyException
	 */
	public void remove(RespuestaPeticionDTO obj)throws MyException ;

}
