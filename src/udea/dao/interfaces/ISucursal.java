/**
 * Interfaz que define los metodos de la clase
 */
package udea.dao.interfaces;

import java.util.List;

import udea.dto.SucursalDTO;
import udea.exception.MyException;

/**
 * @author Stephany Berrio
 *
 */
public interface ISucursal {
	/**
	 * @return una lista de objetos de la clase 
	 * @throws MyException
	 */
	public List<SucursalDTO> getAll() throws MyException ;
	/**
	 * @param id
	 * @return Devuelve el objeto de la clase con el id recibido
	 * @throws MyException
	 */
	public SucursalDTO get(Integer id) throws MyException ;
	/**
	 * Inserta en la base de datos el objeto recibido
	 * @param obj 
	 * @throws MyException
	 */
	public void insert(SucursalDTO obj)throws MyException ;
	/**
	 * Actualiza el objeto recibido, en la base de datos
	 * @param obj
	 * @throws MyException
	 */
	public void update(SucursalDTO obj)throws MyException ;
	/**
	 * Elimina el objeto recibido
	 * @param obj
	 * @throws MyException
	 */
	public void remove(SucursalDTO obj)throws MyException ;

}
