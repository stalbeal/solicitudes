/**
 * Interfaz que define los metodos de la clase
 */
package udea.dao.interfaces;

import java.util.List;

import udea.dto.UsuarioDTO;
import udea.exception.MyException;

/**
 * @author Stephany Berrio
 *
 */
public interface IUsuario {
	/**
	 * @return una lista de objetos de la clase 
	 * @throws MyException
	 */
	public List<UsuarioDTO> getAll() throws MyException ;
	/**
	 * @param id
	 * @return Devuelve el objeto de la clase con el id recibido
	 * @throws MyException
	 */
	public UsuarioDTO get(String username) throws MyException ;
	/**
	 * Inserta en la base de datos el objeto recibido
	 * @param obj 
	 * @throws MyException
	 */
	public void insert(UsuarioDTO obj)throws MyException ;
	/**
	 * Actualiza el objeto recibido, en la base de datos
	 * @param obj
	 * @throws MyException
	 */
	public void update(UsuarioDTO obj)throws MyException ;
	/**
	 * Elimina el objeto recibido
	 * @param obj
	 * @throws MyException
	 */
	public void remove(UsuarioDTO obj)throws MyException ;

}
