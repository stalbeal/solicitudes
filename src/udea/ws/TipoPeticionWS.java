/**
 * 
 */
package udea.ws;

import java.rmi.RemoteException;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import udea.dto.TipoPeticionDTO;
import udea.exception.ASServicesException;
import udea.exception.MyException;
import udea.services.TipoPeticionServicio;


/**
 * @author Stephany Berrio Alzate
 *
 */

@Path("tipopeticiones")
@Component
public class TipoPeticionWS {
	
		
		@Autowired
		private TipoPeticionServicio tipoPeticionServicio;
		
		/**
		 * @return tipoPeticiones
		 * @throws RemoteException
		 * @throws MyException
		 * @throws ASServicesException 
		 */
		@Produces(MediaType.APPLICATION_JSON)
		@GET
		public List<TipoPeticionDTO> getTipoPeticiones() throws RemoteException, MyException, ASServicesException{
			List<TipoPeticionDTO> tipoPeticiones=null;
			try {
			tipoPeticiones=tipoPeticionServicio.getTiposPeticiones();
			}catch(MyException e ) {
				throw new RemoteException(e.getMessage());
			}
			return tipoPeticiones;
		
		}
		
		/**
		 * @param id
		 * @return tipoPeticionDTO
		 * @throws ASServicesException
		 * @throws MyException
		 * @throws RemoteException
		 */
		@Path("tipopeticion")
		@Produces(MediaType.APPLICATION_JSON)
		@GET	
		public TipoPeticionDTO getTipoPeticion(@QueryParam("id") Integer id)
				throws ASServicesException, MyException, RemoteException {
			TipoPeticionDTO tipoPeticionDTO=null;
			try {
				tipoPeticionDTO=tipoPeticionServicio.getTipoPeticion(id);
			} catch (Exception e) {
				throw new RemoteException(e.getMessage());
			}
			return tipoPeticionDTO;

		}
		

}
