/**
 * 
 */
package udea.ws;

import java.rmi.RemoteException;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import udea.dto.UsuarioDTO;
import udea.exception.ASServicesException;
import udea.exception.MyException;
import udea.services.UsuarioServicio;

/**
 * @author Stephany Berrio Alzate
 *
 */

@Path("usuarios")
@Component
public class UsuarioWS {

	@Autowired
	private UsuarioServicio usuarioServicio;

	/**
	 * @return tipoPeticiones
	 * @throws RemoteException
	 * @throws MyException
	 * @throws ASServicesException
	 */
	@Path("login")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public UsuarioDTO logIn(@QueryParam("username") String username,
			@QueryParam("password") String password) throws RemoteException,
			MyException, ASServicesException {
		UsuarioDTO usuario = null;
		try {
			usuario = usuarioServicio.logIn(username, password);
		} catch (MyException e) {
			throw new RemoteException(e.getMessage());
		}
		return usuario;

	}

	/**
	 * @return usuarios
	 * @throws RemoteException
	 * @throws MyException
	 * @throws ASServicesException
	 */
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public List<UsuarioDTO> getUsuarioes() throws RemoteException, MyException,
			ASServicesException {
		List<UsuarioDTO> usuarios = null;
		try {
			usuarios = usuarioServicio.getUsuarios();
		} catch (MyException e) {
			throw new RemoteException(e.getMessage());
		}
		return usuarios;

	}

	/**
	 * @param id
	 * @return tipoPeticionDTO
	 * @throws ASServicesException
	 * @throws MyException
	 * @throws RemoteException
	 */
//	@Path("usuario")
//	@Produces(MediaType.APPLICATION_JSON)
//	@GET
//	public UsuarioDTO getUsuario(@QueryParam("id") Integer id)
//			throws ASServicesException, MyException, RemoteException {
//		UsuarioDTO usuario = null;
//		try {
//			usuario = usuarioServicio.getUsuario(id);
//		} catch (Exception e) {
//			throw new RemoteException(e.getMessage());
//		}
//		return usuario;
//
//	}

}
