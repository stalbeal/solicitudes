/**
 * 
 */
package udea.ws;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import udea.dto.RespuestaPeticionDTO;
import udea.exception.ASServicesException;
import udea.exception.MyException;
import udea.services.RespuestaPeticionServicio;

/**
 * @author Stephany Berrio Alzate
 *
 */

@Path("respuestapeticiones")
@Component
public class RespuestaPeticionWS {

	@Autowired
	private RespuestaPeticionServicio respuestaPeticionServicio;

	/**
	 * @return respuestasPeticiones
	 * @throws RemoteException
	 * @throws MyException
	 * @throws ASServicesException
	 */
	
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public List<RespuestaPeticionDTO> getRespuestaPeticiones() throws RemoteException,
			MyException, ASServicesException {
		List<RespuestaPeticionDTO> respuestasPeticiones = new ArrayList<RespuestaPeticionDTO>();
		try {
			respuestasPeticiones = respuestaPeticionServicio.getRespuestas();
		} catch (MyException e) {
			throw new RemoteException(e.getMessage());
		}
		return respuestasPeticiones;

	}
	
	/**
	 * @param id
	 * @param descripcion
	 * @param peticionId
	 * @param usuarioId
	 * @return string
	 * @throws ASServicesException
	 * @throws MyException
	 * @throws RemoteException
	 */
	@Path("create")
	@Produces(MediaType.APPLICATION_JSON)
	@POST
	public String insertRespuestaPeticion(@QueryParam("id") Integer id,
			@QueryParam("descripcion") String descripcion,			
			@QueryParam("peticionId") Integer peticionId,
			@QueryParam("usuarioId")Integer usuarioId)
			throws ASServicesException, MyException, RemoteException {
		try {
			respuestaPeticionServicio.insertRespuesta(id, descripcion, peticionId, usuarioId);
		} catch (Exception e) {
			throw new RemoteException(e.getMessage());
		}
		return "Exito";

	}
	
	/**
	 * @param id
	 * @return respuestaPeticionDTO
	 * @throws ASServicesException
	 * @throws MyException
	 * @throws RemoteException
	 */
	@Path("respuesta")
	@Produces(MediaType.APPLICATION_JSON)
	@GET	
	public RespuestaPeticionDTO getRespuestaPeticion(@QueryParam("id") Integer id)
			throws ASServicesException, MyException, RemoteException {
		RespuestaPeticionDTO respuestaPeticionDTO=null;
		try {
			respuestaPeticionDTO=respuestaPeticionServicio.getRespuesta(id);
		} catch (Exception e) {
			throw new RemoteException(e.getMessage());
		}
		return respuestaPeticionDTO;

	}

}
