/**
 * 
 */
package udea.ws;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import udea.dto.EvaluacionSatisfaccionDTO;
import udea.exception.ASServicesException;
import udea.exception.MyException;
import udea.services.EvaluacionSatisfaccionServicio;

/**
 * @author Stephany Berrio Alzate
 *
 */

@Path("evaluacion")
@Component
public class EvaluacionSatisfaccionWS {

	@Autowired
	private EvaluacionSatisfaccionServicio evaluacionSatisfaccionServicio;

	
	
	/**
	 * @return respuestaspeticiones
	 * @throws RemoteException
	 * @throws MyException
	 * @throws ASServicesException
	 */
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public List<EvaluacionSatisfaccionDTO> getEvaluaciones() throws RemoteException,
			MyException, ASServicesException {
		List<EvaluacionSatisfaccionDTO> respuestasPeticiones = new ArrayList<EvaluacionSatisfaccionDTO>();
		try {
			respuestasPeticiones = evaluacionSatisfaccionServicio.getEvaluaciones();
		} catch (MyException e) {
			throw new RemoteException(e.getMessage());
		}
		return respuestasPeticiones;

	}
	
	
	/**
	 * @param agrado
	 * @param id
	 * @param respuestaPeticion
	 * @param visitaSucursal
	 * @return string
	 * @throws ASServicesException
	 * @throws MyException
	 * @throws RemoteException
	 */
	@Path("create")
	@Produces(MediaType.APPLICATION_JSON)
	@POST
	public String insertEvaluacionSatisfaccion(@QueryParam("agrado") String agrado,	
			@QueryParam("id") Integer id,
			@QueryParam("respuestaPeticion") Integer respuestaPeticion,
			@QueryParam("visitaSucursal")String visitaSucursal)
			throws ASServicesException, MyException, RemoteException {
		try {
			evaluacionSatisfaccionServicio.insertEvaluacion(agrado, id, respuestaPeticion, visitaSucursal);
			} catch (Exception e) {
			throw new RemoteException(e.getMessage());
		}
		return "Exito";

	}
	
	/**
	 * @param id
	 * @return evaluacionSatisfaccionDTO
	 * @throws ASServicesException
	 * @throws MyException
	 * @throws RemoteException
	 */
	@Path("evaluacion")
	@Produces(MediaType.APPLICATION_JSON)
	@GET	
	public EvaluacionSatisfaccionDTO getEvaluacionSatisfaccion(@QueryParam("id") Integer id)
			throws ASServicesException, MyException, RemoteException {
		EvaluacionSatisfaccionDTO evaluacionSatisfaccionDTO=null;
		try {
			evaluacionSatisfaccionDTO=evaluacionSatisfaccionServicio.getEvaluacion(id);
		} catch (Exception e) {
			throw new RemoteException(e.getMessage());
		}
		return evaluacionSatisfaccionDTO;

	}

}
