/**
 * 
 */
package udea.ws;

import java.rmi.RemoteException;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import udea.dto.SucursalDTO;
import udea.exception.ASServicesException;
import udea.exception.MyException;
import udea.services.SucursalServicio;


/**
 * @author Stephany Berrio Alzate
 *
 */

@Path("sucursales")
@Component
public class SucursalWS {
	
		
		@Autowired
		private SucursalServicio sucursalServicio;
		
		/**
		 * @return sucursales
		 * @throws RemoteException
		 * @throws MyException
		 * @throws ASServicesException 
		 */
		@Produces(MediaType.APPLICATION_JSON)
		@GET
		public List<SucursalDTO> getSucursales() throws RemoteException, MyException, ASServicesException{
			List<SucursalDTO> sucursales=null;
			try {
			sucursales=sucursalServicio.getSucursales();
			}catch(MyException e ) {
				throw new RemoteException(e.getMessage());
			}
			return sucursales;
		
		}
		
		/**
		 * @param id
		 * @return
		 * @throws ASServicesException
		 * @throws MyException
		 * @throws RemoteException
		 */
		@Path("sucursal")
		@Produces(MediaType.APPLICATION_JSON)
		@GET	
		public SucursalDTO getSucursal(@QueryParam("id") Integer id)
				throws ASServicesException, MyException, RemoteException {
			SucursalDTO sucursalDTO=null;
			try {
				sucursalDTO=sucursalServicio.getSucursal(id);
			} catch (Exception e) {
				throw new RemoteException(e.getMessage());
			}
			return sucursalDTO;

		}
		

}
