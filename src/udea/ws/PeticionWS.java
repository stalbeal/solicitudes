/**
 * 
 */
package udea.ws;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import udea.dto.PeticionDTO;
import udea.exception.ASServicesException;
import udea.exception.MyException;
import udea.services.PeticionServicio;

/**
 * @author Stephany Berrio Alzate
 *
 */

@Path("peticiones")
@Component
public class PeticionWS {

	@Autowired
	private PeticionServicio peticionServicio;

	/**
	 * @return peticiones
	 * @throws RemoteException
	 * @throws MyException
	 * @throws ASServicesException
	 */
	
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public List<PeticionDTO> getPeticiones() throws RemoteException,
			MyException, ASServicesException {
		List<PeticionDTO> peticiones = new ArrayList<PeticionDTO>();
		try {
			peticiones = peticionServicio.getPeticiones();
		} catch (MyException e) {
			throw new RemoteException(e.getMessage());
		}
		return peticiones;

	}
	/**
	 * @param id
	 * @param nombres
	 * @param apellidos
	 * @param email
	 * @param telefono
	 * @param celular
	 * @param descripcion
	 * @param tipoPeticionId
	 * @param productoId
	 * @param sucursalId
	 * @return
	 * @throws ASServicesException
	 * @throws MyException
	 * @throws RemoteException
	 */
	@Path("create")
	@Produces(MediaType.APPLICATION_JSON)
	@POST
	public String insertPeticion(@QueryParam("id") Integer id,
			@QueryParam("nombres") String nombres,
			@QueryParam("apellidos") String apellidos,
			@QueryParam("email") String email,
			@QueryParam("telefono") String telefono,
			@QueryParam("celular") String celular,
			@QueryParam("descripcion") String descripcion,
			@QueryParam("tipoPeticionId") Integer tipoPeticionId,
			@QueryParam("productoId") Integer productoId,
			@QueryParam("sucursalId")Integer sucursalId)
			throws ASServicesException, MyException, RemoteException {
		try {
			
			
			peticionServicio.insertPeticion(id, nombres, apellidos, email,
					telefono, celular,descripcion, tipoPeticionId, productoId,
					sucursalId);
		} catch (Exception e) {
			throw new RemoteException(e.getMessage());
		}
		return "Exito";

	}
	
	/**
	 * @param id
	 * @return
	 * @throws ASServicesException
	 * @throws MyException
	 * @throws RemoteException
	 */
	@Path("peticion")
	@Produces(MediaType.APPLICATION_JSON)
	@GET	
	public PeticionDTO getPeticion(@QueryParam("id") Integer id)
			throws ASServicesException, MyException, RemoteException {
		PeticionDTO peticionDTO=null;
		try {
			peticionDTO=peticionServicio.getPeticion(id);
		} catch (Exception e) {
			throw new RemoteException(e.getMessage());
		}
		return peticionDTO;

	}

}
