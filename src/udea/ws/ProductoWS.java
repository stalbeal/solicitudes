/**
 * 
 */
package udea.ws;

import java.rmi.RemoteException;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import udea.dto.ProductoDTO;
import udea.exception.ASServicesException;
import udea.exception.MyException;
import udea.services.ProductoServicio;


/**
 * @author Stephany Berrio Alzate
 *
 */

@Path("productos")
@Component
public class ProductoWS {
	
		
		@Autowired
		private ProductoServicio productoServicio;
		
		/**
		 * @return productos
		 * @throws RemoteException
		 * @throws MyException
		 */
		@Produces(MediaType.APPLICATION_JSON)
		@GET
		public List<ProductoDTO> getProductos() throws RemoteException, MyException{
			List<ProductoDTO> productos=null;
			try {
			productos=productoServicio.getProductos();
			}catch(MyException e ) {
				throw new RemoteException(e.getMessage());
			}
			return productos;
		
		}
		
		/**
		 * @param id
		 * @return
		 * @throws ASServicesException
		 * @throws MyException
		 * @throws RemoteException
		 */
		@Path("producto")
		@Produces(MediaType.APPLICATION_JSON)
		@GET	
		public ProductoDTO getProducto(@QueryParam("id") Integer id)
				throws ASServicesException, MyException, RemoteException {
			ProductoDTO productoDTO=null;
			try {
				productoDTO=productoServicio.getProducto(id);
			} catch (Exception e) {
				throw new RemoteException(e.getMessage());
			}
			return productoDTO;

		}
		

}
