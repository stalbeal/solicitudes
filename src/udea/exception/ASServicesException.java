package udea.exception;

public class ASServicesException extends Exception{

	public ASServicesException() {
		// TODO Auto-generated constructor stub
	}

	public ASServicesException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ASServicesException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ASServicesException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ASServicesException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}


}
