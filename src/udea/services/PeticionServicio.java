/**
 * 
 */
package udea.services;

import java.util.Date;
import java.util.List;

import udea.dao.implement.PeticionImp;
import udea.dto.PeticionDTO;
import udea.dto.ProductoDTO;
import udea.dto.SucursalDTO;
import udea.dto.TipoPeticionDTO;
import udea.exception.ASServicesException;
import udea.exception.MyException;
import udea.util.Validar;

/**
 * @author Stephany Berrio Alzate
 *
 */
public class PeticionServicio {
	//Objeto de tipo PeticionImp
		private PeticionImp peticionImp;
		
		/**
		 * @return todas las peticiones
		 * @throws ASServicesException
		 * @throws MyException
		 */
		public List<PeticionDTO> getPeticiones() throws ASServicesException, MyException{
			return peticionImp.getAll();
		}
		
		/**
		 * Servicio para crear una peticion
		 * @param id
		 * @param nombres
		 * @param apellidos
		 * @param email
		 * @param telefono
		 * @param celular
		 * @param descripcion
		 * @param fechaSolicitud
		 * @param tipoPeticionId
		 * @param productoId
		 * @param sucursalId
		 * @throws ASServicesException
		 * @throws MyException
		 */
		public void insertPeticion(Integer id, String nombres, String apellidos,
				String email, String telefono, String celular, String descripcion,
				Integer tipoPeticionId, Integer productoId,
				Integer sucursalId) throws ASServicesException, MyException {
			PeticionDTO peticion=new PeticionDTO();
			if(id == null || "".equals(id)){
				throw new ASServicesException("El id no puede ser nulo");
			}
			if(!Validar.isNull(nombres)){
				throw new ASServicesException("Debe ingresar los nombres");
			}
			if(!Validar.isNull(apellidos)){
				throw new ASServicesException("Debe ingresar los apellidos");
			}
			if(!Validar.isNull(email)){
				throw new ASServicesException("Debe ingresar el email");
			}
			if(!Validar.isNull(telefono)){
				throw new ASServicesException("Debe ingresar el telefono");
			}
			
		    ProductoDTO p=new ProductoDTO();
		    p.setId(productoId);
		    SucursalDTO s=new SucursalDTO();
		    s.setId(sucursalId);
		    TipoPeticionDTO t= new TipoPeticionDTO();
		    t.setId(tipoPeticionId);
			peticion.setId(id);
			peticion.setNombres(nombres);
			peticion.setApellidos(apellidos);
			peticion.setEmail(email);
			peticion.setTelefono(telefono);
			peticion.setCelular(celular);
			peticion.setDescripcion(descripcion);
			peticion.setFechaSolicitud(new Date());
			peticion.setEstadoPeticion(false);
			peticion.setTipoPeticionId(t);
			peticion.setProductoId(p);
			peticion.setSucursalId(s);
			
			peticionImp.insert(peticion);
			
		}

		/**
		 * @param id
		 * @return una peticion
		 * @throws MyException
		 */
		public PeticionDTO getPeticion(Integer id) throws MyException {
			
			return peticionImp.get(id);
		}

		/**
		 * @return the peticionImp
		 */
		public PeticionImp getPeticionImp() {
			return peticionImp;
		}

		/**
		 * @param peticionImp the peticionImp to set
		 */
		public void setPeticionImp(PeticionImp peticionImp) {
			this.peticionImp = peticionImp;
		}
		
		
		
		
		
		
}
