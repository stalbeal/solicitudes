/**
 * 
 */
package udea.services;


import java.util.List;

import udea.dao.implement.UsuarioImp;
import udea.dto.UsuarioDTO;
import udea.exception.ASServicesException;
import udea.exception.MyException;

/**
 * @author Stephany Berrio Alzate
 *
 */
public class UsuarioServicio {
	private UsuarioImp usuarioImp;

	/**
	 * @param id
	 * @param password
	 * @return user
	 * @throws ASServicesException
	 * @throws MyException
	 */
	public UsuarioDTO logIn(String username, String password)
			throws ASServicesException, MyException {

		UsuarioDTO user = null;
		if (password == null || "".equals(password)) {
			throw new ASServicesException("Debe Ingresar Contraseña");
		}

		user = usuarioImp.get(username);
		if (user != null && user.getContrasena().equals(password)) {
			return user;
		} else {
			throw new ASServicesException("Datos Erroneos Verifique Sus Datos");
		}
	}
	
	
	
	/**
	 * Servicio para obtener usuarios
	 * @param usuarioId 
	 * @return 
	 * @return usuarioImp
	 * @throws MyException 
	 */
	public  List<UsuarioDTO> getUsuarios() throws MyException {
		List<UsuarioDTO> lista=usuarioImp.getAll();
		return lista ;
	}

	

	/**
	 * @return the usuarioImp
	 */
	public UsuarioImp getUsuarioImp() {
		return usuarioImp;
	}

	/**
	 * @param usuarioImp the usuarioImp to set
	 */
	public void setUsuarioImp(UsuarioImp usuarioImp) {
		this.usuarioImp = usuarioImp;
	}
	
	

}
