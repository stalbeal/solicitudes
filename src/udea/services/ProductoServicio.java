/**
 * 
 */
package udea.services;

import java.util.List;

import udea.dao.implement.ProductoImp;
import udea.dto.ProductoDTO;
import udea.exception.MyException;

/**
 * @author Stephany Berrio Alzate
 *
 */
public class ProductoServicio {

	//Objeto de tipo ProductoImp
		private ProductoImp productosImp;
		
		
		/**
		 * @return todas los productos
		 * @throws ASServicesException
		 * @throws MyException
		 */
		public List<ProductoDTO> getProductos() throws MyException{
			return productosImp.getAll();
		}
		
		/**
		 * @param id
		 * @return un producto
		 * @throws MyException
		 */
		public ProductoDTO getProducto(Integer id) throws MyException{
			return productosImp.get(id);
		}

		/**
		 * @return the productosImp
		 */
		public ProductoImp getProductosImp() {
			return productosImp;
		}

		/**
		 * @param productosImp the productosImp to set
		 */
		public void setProductosImp(ProductoImp productosImp) {
			this.productosImp = productosImp;
		}
		
		
		
		
}
