/**
 * 
 */
package udea.services;

import java.util.List;

import udea.dao.implement.SucursalImp;
import udea.dto.SucursalDTO;
import udea.exception.ASServicesException;
import udea.exception.MyException;

/**
 * @author Stephany Berrio Alzate
 *
 */
public class SucursalServicio {
	//Objeto de tipo SucursalImp
	private SucursalImp sucursalImp;
	
	/**
	 * @return todas las sucursalImp
	 * @throws ASServicesException
	 * @throws MyException
	 */
	public List<SucursalDTO> getSucursales() throws ASServicesException, MyException{
		return sucursalImp.getAll();
	}
	
	/**
	 * Retorna una sucursal según su id
	 * @param id
	 * @return una sucursal
	 * @throws MyException
	 */
	public SucursalDTO getSucursal(Integer id) throws MyException{
		return sucursalImp.get(id);
	}

	/**
	 * @return the sucursalImp
	 */
	public SucursalImp getSucursalImp() {
		return sucursalImp;
	}

	/**
	 * @param sucursalImp the sucursalImp to set
	 */
	public void setSucursalImp(SucursalImp sucursalImp) {
		this.sucursalImp = sucursalImp;
	}
	
	

}
