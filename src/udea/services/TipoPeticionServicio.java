/**
 * 
 */
package udea.services;

import java.util.List;

import udea.dao.implement.TipoPeticionImp;
import udea.dto.TipoPeticionDTO;
import udea.exception.ASServicesException;
import udea.exception.MyException;

/**
 * @author Stephany Berrio Alzate
 *
 */
public class TipoPeticionServicio {
	
	//Objeto de tipo TipoPeticionImp
		private TipoPeticionImp tipoPeticionImp;
		
		/**
		 * @return todas los tipoPeticionImp de peticiones
		 * @throws ASServicesException
		 * @throws MyException
		 */
		public List<TipoPeticionDTO> getTiposPeticiones() throws ASServicesException, MyException{
			return tipoPeticionImp.getAll();
		}
		
		/**
		 * @param id
		 * @return un tipo de peticion
		 * @throws MyException
		 */
		public TipoPeticionDTO getTipoPeticion(Integer id) throws MyException{
			return tipoPeticionImp.get(id);
		}

		/**
		 * @return the tipoPeticionImp
		 */
		public TipoPeticionImp getTipoPeticionImp() {
			return tipoPeticionImp;
		}

		/**
		 * @param tipoPeticionImp the tipoPeticionImp to set
		 */
		public void setTipoPeticionImp(TipoPeticionImp tipoPeticionImp) {
			this.tipoPeticionImp = tipoPeticionImp;
		}
		
		

}
