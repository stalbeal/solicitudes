/**
 * 
 */
package udea.services;

import java.util.Date;
import java.util.List;

import udea.dao.implement.RespuestaPeticionImp;
import udea.dto.PeticionDTO;
import udea.dto.RespuestaPeticionDTO;
import udea.dto.UsuarioDTO;
import udea.exception.ASServicesException;
import udea.exception.MyException;
import udea.util.Validar;

/**
 * @author Stephany Berrio Alzate
 *
 */
public class RespuestaPeticionServicio {
	String SUBJECT="RESPUETA SOLICITUD";
	RespuestaPeticionImp respuestaPeticionImp=new RespuestaPeticionImp();

	public void insertRespuesta(Integer id, String descripcion,Integer peticionId, Integer usuarioId) throws ASServicesException, MyException {
		//link para la encuesta de satisfaccion
		String link="";
		if(!Validar.isNull(id)){
			throw new ASServicesException("El id no puede ser nulo");
		}
		if(!Validar.isNull(descripcion)){
			throw new ASServicesException("Debe ingresar la descripción");
		}
		if(!Validar.isNull(peticionId)){
			throw new ASServicesException("El id de la peticion no puede ser nulo");
		}
		
		PeticionDTO p=new PeticionDTO();
		p.setId(peticionId);
		
		UsuarioDTO user=new UsuarioDTO();
		user.setId(usuarioId);
		
		RespuestaPeticionDTO res=new RespuestaPeticionDTO();
		res.setDescripcion(descripcion);
		res.setFechaRespuesta(new Date());
		res.setId(id);
		res.setPeticionId(p);
		res.setUsuarioId(user);
		
		respuestaPeticionImp.insert(res);
		descripcion=descripcion+" \n Si desea puede contestar una encuesta para evaluar su satisfacción en el siguiente link"+link;
		//Servicio para envio de la peticion
		//EmailServicio.sendEmail("stalbeal10@gmail.com", "password", user.getEmail(), SUBJECT, descripcion);
		
		
	}
	
	/**
	 * @return Lista RespuestaPeticionDTO
	 * @throws MyException
	 */
	public List<RespuestaPeticionDTO> getRespuestas() throws MyException{
		return respuestaPeticionImp.getAll();
	}
	
	/**
	 * @param id
	 * @return RespuestaPeticionDTO
	 * @throws MyException
	 */
	public RespuestaPeticionDTO getRespuesta(Integer id) throws MyException{
		return respuestaPeticionImp.get(id);
	}

	/**
	 * @return the respuestaPeticionImp
	 */
	public RespuestaPeticionImp getRespuestaPeticionImp() {
		return respuestaPeticionImp;
	}

	/**
	 * @param respuestaPeticionImp the respuestaPeticionImp to set
	 */
	public void setRespuestaPeticionImp(RespuestaPeticionImp respuestaPeticionImp) {
		this.respuestaPeticionImp = respuestaPeticionImp;
	}
	
	
	
	
	
	

}
