/**
 * 
 */
package udea.services;

import java.util.List;

import udea.dao.implement.EvaluacionSatisfaccionImp;
import udea.dto.EvaluacionSatisfaccionDTO;
import udea.dto.RespuestaPeticionDTO;
import udea.exception.ASServicesException;
import udea.exception.MyException;
import udea.util.Validar;

/**
 * @author Stephany Berrio Alzate
 *
 */
public class EvaluacionSatisfaccionServicio {

	EvaluacionSatisfaccionImp evaluacionImp;


	/**
	 * @return the evaluacionImp
	 */
	public EvaluacionSatisfaccionImp getEvaluacionImp() {
		return evaluacionImp;
	}

	/**
	 * @param evaluacionImp the evaluacionImp to set
	 */
	public void setEvaluacionImp(EvaluacionSatisfaccionImp evaluacionImp) {
		this.evaluacionImp = evaluacionImp;
	}

	/**
	 * Servicio para insertar una evaluacionImp
	 * @param agrado
	 * @param id
	 * @param respuestaPeticion
	 * @param visitaSucursal
	 * @throws ASServicesException
	 * @throws MyException
	 */
	public void insertEvaluacion(String agrado, Integer id,
			Integer respuestaPeticion, String visitaSucursal)
			throws ASServicesException, MyException {
		if (!Validar.isNull(id)) {
			throw new ASServicesException("El id no puede ser nulo");
		}
		if (!Validar.isNull(respuestaPeticion)) {
			throw new ASServicesException(
					"El id de la respuesta no puede ser nulo");
		}
		RespuestaPeticionDTO res = new RespuestaPeticionDTO();
		res.setId(respuestaPeticion);
		EvaluacionSatisfaccionDTO eval = new EvaluacionSatisfaccionDTO();
		eval.setAgrado(agrado);
		eval.setVisitaSucursal(visitaSucursal);
		eval.setRespuestaPeticion(res);
		eval.setId(id);

		evaluacionImp.insert(eval);

	}
	
	/**
	 * Servicio para obtener una evaluacionImp
	 * @param id
	 * @return una evaluacionImp
	 * @throws MyException
	 */
	public EvaluacionSatisfaccionDTO getEvaluacion(Integer id) throws MyException{
		return evaluacionImp.get(id);
	}
	
	/**
	 * Servicio para obtener todas las evaluacionImpes
	 * @return todas las evaluacionImpes de satsfaccion
	 * @throws MyException
	 */
	public List<EvaluacionSatisfaccionDTO> getEvaluaciones() throws MyException{
		return evaluacionImp.getAll();
	}
	
	
	
	
	
}
