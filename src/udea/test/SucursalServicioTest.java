/**
 * 
 */
package udea.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import udea.dto.SucursalDTO;
import udea.exception.ASServicesException;
import udea.exception.MyException;
import udea.services.SucursalServicio;

/**
 * @author Stephany Berrio Alzate
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Component
@ContextConfiguration(locations = "classpath:springconfig.xml")
public class SucursalServicioTest {
	@Autowired
	SucursalServicio sucursalServicio;

	/**
	 * Test method for {@link udea.services.SucursalServicio#getSucursales()}.
	 */
	@Test
	public void testGetSucursales() {
		List<SucursalDTO> lista;
		try {
			lista = sucursalServicio.getSucursales();
			assertTrue(true);
		} catch (ASServicesException e) {
			e.printStackTrace();
		} catch (MyException e) {
			e.printStackTrace();
		}

	}



	/**
	 * Test method for
	 * {@link udea.services.SucursalServicio#getSucursal(java.lang.Integer)}.
	 */
	@Test
	public void testGetSucursal() {
		SucursalDTO peticion;
		try {
			peticion = sucursalServicio.getSucursal(1);
			assertTrue(true);
		} catch (MyException e) {
			e.printStackTrace();
		}
	}

}
