/**
 * 
 */
package udea.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import udea.dto.EvaluacionSatisfaccionDTO;
import udea.exception.ASServicesException;
import udea.exception.MyException;
import udea.services.EvaluacionSatisfaccionServicio;

/**
 * @author Stephany Berrio Alzate
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Component
@ContextConfiguration(locations = "classpath:springconfig.xml")
public class EvaluacionSatisfaccionServicioTest {
	@Autowired
	EvaluacionSatisfaccionServicio evaluacionSatisfaccionServicio;

	/**
	 * Test method for
	 * {@link udea.services.EvaluacionSatisfaccionServicio#insertEvaluacion(java.lang.Integer, java.lang.String, java.lang.Integer, java.lang.Integer)}
	 * .
	 */
	// @Test
	public void testInsertEvaluacion() {
		try {
			evaluacionSatisfaccionServicio.insertEvaluacion("Si", 100, 100,
					"Si");
			assertTrue(true);
		} catch (ASServicesException | MyException e) {
			fail(e.getMessage());
		}

	}

	/**
	 * Test method for
	 * {@link udea.services.EvaluacionSatisfaccionServicio#getEvaluaciones()}.
	 */
	@Test
	public void testGetEvaluaciones() {
		List<EvaluacionSatisfaccionDTO> lista;
		try {
			lista = evaluacionSatisfaccionServicio.getEvaluaciones();
			assertTrue(true);
		} catch (MyException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link udea.services.EvaluacionSatisfaccionServicio#getEvaluacion(java.lang.Integer)}
	 * .
	 */
	@Test
	public void testGetEvaluacion() {
		EvaluacionSatisfaccionDTO res;
		try {
			res = evaluacionSatisfaccionServicio.getEvaluacion(1);
			assertTrue(true);
		} catch (MyException e) {
			fail(e.getMessage());
		}
	}

}
