/**
 * 
 */
package udea.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import udea.dto.ProductoDTO;
import udea.exception.MyException;
import udea.services.ProductoServicio;

/**
 * @author Stephany Berrio Alzate
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Component
@ContextConfiguration(locations = "classpath:springconfig.xml")
public class ProductoServicioTest {
	@Autowired
	ProductoServicio productoServicio;
	/**
	 * Test method for {@link udea.services.ProductoServicio#getProductos()}.
	 * @throws MyException 
	 */
	@Test
	public void testGetProductos() throws MyException {
		List<ProductoDTO> lista=productoServicio.getProductos();
		assertNotNull(lista);
		
	}

}
