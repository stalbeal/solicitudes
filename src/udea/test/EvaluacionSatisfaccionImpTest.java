/**
 * Prueba unitaria para EvaluacionSatisfaccionImp
 * ver udea.dao.implement.EvaluacionSatisfaccionImp
 */

package udea.test;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import udea.dao.implement.EvaluacionSatisfaccionImp;
import udea.dto.EvaluacionSatisfaccionDTO;
import udea.dto.ProductoDTO;
import udea.dto.RespuestaPeticionDTO;
import udea.dto.SucursalDTO;
import udea.exception.MyException;

@RunWith(SpringJUnit4ClassRunner.class)
@Component
@ContextConfiguration(locations = "classpath:springconfig.xml")
/**
 * @author Stephany Berrio Alzate
 *
 */
public class EvaluacionSatisfaccionImpTest {

	/**
	 * Test method for
	 * {@link udea.dao.implement.EvaluacionSatisfaccionImp#insert(udea.dto.EvaluacionSatisfaccionDTO)}.
	 */
	@Autowired
	EvaluacionSatisfaccionImp prod;

	@Test
	public void testInsert() {

		try {
			
			Random rnd = new Random();
			int num = (int) (rnd.nextDouble() * 100);
			EvaluacionSatisfaccionDTO p = new EvaluacionSatisfaccionDTO();
			p.setAgrado("actualice");
			p.setId(num);		
			p.setVisitaSucursal("si");
			RespuestaPeticionDTO  pa= new RespuestaPeticionDTO();
			pa.setId(1);
			p.setRespuestaPeticion(pa);
			prod.insert(p);
			assertTrue(true);

		} catch (Exception e) {
			fail(e.getMessage());

		}
	}

	/**a
	 * Test method for
	 * {@link udea.dao.implement.EvaluacionSatisfaccionImp#update(udea.dto.EvaluacionSatisfaccionDTO)}.
	 */
	@Test
	public void testUpdate() {
		try {
			EvaluacionSatisfaccionDTO p = new EvaluacionSatisfaccionDTO();
			p.setAgrado("actualice");
			p.setId(2);		
			p.setVisitaSucursal("si");
			RespuestaPeticionDTO  pa= new RespuestaPeticionDTO();
			pa.setId(1);
			p.setRespuestaPeticion(pa);
		
			prod.update(p);
			assertTrue(true);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link udea.dao.implement.EvaluacionSatisfaccionImp#remove(udea.dto.EvaluacionSatisfaccionDTO)}.
	 */
	@Test
	public void testRemove() {
		try {
			EvaluacionSatisfaccionDTO p = new EvaluacionSatisfaccionDTO();
			
			p.setId(3);
			prod.remove(p);
			assertTrue(true);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for {@link udea.dao.implement.EvaluacionSatisfaccionImp#getAll()}.
	 */
	@Test
	public void testGetAll() {

		try {
			List<EvaluacionSatisfaccionDTO> evaluaciones = prod.getAll();
			for (EvaluacionSatisfaccionDTO d : evaluaciones) {
				System.out.println(d.getVisitaSucursal());
			}

			assertTrue(true);

		} catch (MyException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link udea.dao.implement.EvaluacionSatisfaccionImp#get(java.lang.Integer)}.
	 */
	@Test
	public void testGet() {
		try {
			EvaluacionSatisfaccionDTO evaluacion = prod.get(2);
			System.out.println("nombre="+evaluacion.getAgrado());
			assertTrue(true);

		} catch (MyException e) {
			fail(e.getMessage());
		}
	}

}
