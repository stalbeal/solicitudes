/**
 * 
 */
package udea.test;

import static org.junit.Assert.*;

import java.rmi.RemoteException;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import udea.dto.TipoPeticionDTO;
import udea.dto.ProductoDTO;
import udea.exception.ASServicesException;
import udea.exception.MyException;
import udea.services.TipoPeticionServicio;

/**
 * @author Stephany Berrio Alzate
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Component
@ContextConfiguration(locations = "classpath:springconfig.xml")
public class TipoPeticionServicioTest {
	@Autowired
	TipoPeticionServicio tipoPeticionServicio;

	/**
	 * Test method for {@link udea.services.TipoPeticionServicio#getTipoPeticiones()}.
	 */
	@Test
	public void testGetTipoPeticiones() {
		List<TipoPeticionDTO> lista;
		try {
			lista = tipoPeticionServicio.getTiposPeticiones();
			assertTrue(true);
		} catch (ASServicesException e) {
			e.printStackTrace();
		} catch (MyException e) {
			e.printStackTrace();
		}

	}



	/**
	 * Test method for
	 * {@link udea.services.TipoPeticionServicio#getTipoPeticion(java.lang.Integer)}.
	 */
	@Test
	public void testGetTipoPeticion() {
		TipoPeticionDTO tipo;
		try {
			tipo = tipoPeticionServicio.getTipoPeticion(1);
			assertTrue(true);
		} catch (MyException e) {
			e.printStackTrace();
		}
	}

}
