package udea.test;


import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import udea.dao.implement.RespuestaPeticionImp;
import udea.dto.PeticionDTO;
import udea.dto.RespuestaPeticionDTO;
import udea.dto.UsuarioDTO;
import udea.exception.MyException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:springconfig.xml")
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class RespuestaPeticionImpTest {

    @Autowired
    RespuestaPeticionImp resp;

    @Test
    public void testInsert() {

        try {

            PeticionDTO p = new PeticionDTO();
            p.setId(1);
            RespuestaPeticionDTO respuestaPeticionDTO = new RespuestaPeticionDTO();
            respuestaPeticionDTO.setDescripcion("Muy mala Esa ");
            respuestaPeticionDTO.setFechaRespuesta(new Date());
            respuestaPeticionDTO.setPeticionId(p);
            UsuarioDTO u = new UsuarioDTO();
            u.setId(1);
            respuestaPeticionDTO.setUsuarioId(u);
            respuestaPeticionDTO.setId(5);
            resp.insert(respuestaPeticionDTO);

            assertTrue(true);
        } catch (MyException e) {
            fail(e.getMessage());
        }

    }

    @Test
    public void testGetAll() {

        try {
            List<RespuestaPeticionDTO> respuestaPeticionDTOs = resp.getAll();
            for (RespuestaPeticionDTO r : respuestaPeticionDTOs)
                System.out.println(r.getDescripcion());

            assertTrue(true);

        } catch (MyException e) {
            fail(e.getMessage());
        }

    }

    @Test
    public void testUpdate() {
        try {

            PeticionDTO p = new PeticionDTO();
            p.setId(1);
            RespuestaPeticionDTO respuestaPeticionDTO = new RespuestaPeticionDTO();
            respuestaPeticionDTO.setDescripcion("No esta tan mala");
            respuestaPeticionDTO.setFechaRespuesta(new Date());
            respuestaPeticionDTO.setPeticionId(p);
            UsuarioDTO u = new UsuarioDTO();
            u.setId(1);
            respuestaPeticionDTO.setUsuarioId(u);
            respuestaPeticionDTO.setId(1);
            resp.update(respuestaPeticionDTO);
            assertTrue(true);

        } catch (MyException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testDelete() {
        try {

            RespuestaPeticionDTO respuestaPeticionDTO = new RespuestaPeticionDTO();
            respuestaPeticionDTO.setId(4);
            resp.remove(respuestaPeticionDTO);
            assertTrue(true);

        } catch (MyException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testGetOne() {
        try {

            RespuestaPeticionDTO respuestaPeticionDTO = resp.get(2);
            System.out.println(respuestaPeticionDTO.getDescripcion());
            assertTrue(true);

        } catch (MyException e) {
            fail(e.getMessage());
        }
    }

}
