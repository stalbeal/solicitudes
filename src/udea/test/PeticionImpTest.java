/**
 * Prueba unitaria para PeticionImp
 * ver udea.dao.implement.PeticionImp
 */

package udea.test;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import udea.dao.implement.PeticionImp;
import udea.dto.PeticionDTO;
import udea.dto.ProductoDTO;
import udea.dto.SucursalDTO;
import udea.dto.TipoPeticionDTO;
import udea.exception.MyException;

@RunWith(SpringJUnit4ClassRunner.class)
@Component
@ContextConfiguration(locations = "classpath:springconfig.xml")
/**
 * @author Stephany Berrio Alzate
 *
 */
public class PeticionImpTest {

	/**
	 * Test method for
	 * {@link udea.dao.implement.PeticionImp#insert(udea.dto.PeticionDTO)}.
	 */
	@Autowired
	PeticionImp prod;

	//@Test
	public void testInsert() {

		try {
			
			Random rnd = new Random();
			int num = (int) (rnd.nextDouble() * 100);
			PeticionDTO p = new PeticionDTO();
			p.setApellidos("algo");
			p.setId(2);
			p.setNombres("algo");
			p.setCelular("algo");
			p.setEmail("algo");
			p.setEstadoPeticion(false);
			p.setTelefono("5453235");
			p.setDescripcion("algo");
			p.setFechaSolicitud(new Date());
			ProductoDTO  pa= new ProductoDTO();
			pa.setId(num);
			p.setProductoId(pa);
			SucursalDTO s=new SucursalDTO();
			s.setId(1);
			TipoPeticionDTO t=new TipoPeticionDTO();
			t.setId(1);
			p.setSucursalId(s);
			p.setTipoPeticionId(t);
			prod.insert(p);
			assertTrue(true);

		} catch (Exception e) {
			fail(e.getMessage());

		}
	}

	/**a
	 * Test method for
	 * {@link udea.dao.implement.PeticionImp#update(udea.dto.PeticionDTO)}.
	 */
	//@Test
	public void testUpdate() {
		try {
			PeticionDTO p = new PeticionDTO();
			p.setApellidos("actualice");
			p.setId(2);
			p.setNombres("algo");
			p.setCelular("algo");
			p.setEmail("algo");
			p.setEstadoPeticion(false);
			p.setTelefono("5453235");
			p.setDescripcion("algo");
			p.setFechaSolicitud(new Date());
			ProductoDTO  pa= new ProductoDTO();
			pa.setId(1);
			p.setProductoId(pa);
			SucursalDTO s=new SucursalDTO();
			s.setId(1);
			TipoPeticionDTO t=new TipoPeticionDTO();
			t.setId(1);
			p.setSucursalId(s);
			p.setTipoPeticionId(t);
			
			prod.update(p);
			assertTrue(true);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link udea.dao.implement.PeticionImp#remove(udea.dto.PeticionDTO)}.
	 */
	//@Test
	public void testRemove() {
		try {
			PeticionDTO p = new PeticionDTO();
			
			p.setId(3);
			prod.remove(p);
			assertTrue(true);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for {@link udea.dao.implement.PeticionImp#getAll()}.
	 */
	//@Test
	public void testGetAll() {

		try {
			List<PeticionDTO> peticiones = prod.getAll();
			for (PeticionDTO d : peticiones) {
				System.out.println(d.getNombres());
			}

			assertTrue(true);

		} catch (MyException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link udea.dao.implement.PeticionImp#get(java.lang.Integer)}.
	 */
	@Test
	public void testGet() {
		try {
			PeticionDTO peticion = prod.get(2);
			System.out.println("nombre="+peticion.getNombres());
			assertTrue(true);

		} catch (MyException e) {
			fail(e.getMessage());
		}
	}

}
