/**
 * 
 */
package udea.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import udea.dto.RespuestaPeticionDTO;
import udea.exception.ASServicesException;
import udea.exception.MyException;
import udea.services.RespuestaPeticionServicio;

/**
 * @author Stephany Berrio Alzate
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Component
@ContextConfiguration(locations = "classpath:springconfig.xml")
public class RespuestaPeticionServicioTest {
	@Autowired
	RespuestaPeticionServicio respuestaPeticionServicio;
	/**
	 * Test method for {@link udea.services.RespuestaPeticionServicio#insertRespuesta(java.lang.Integer, java.lang.String, java.lang.Integer, java.lang.Integer)}.
	 */
	@Test
	public void testInsertRespuesta() {
		try {
			respuestaPeticionServicio.insertRespuesta(100, "desc",100, 1);
			assertTrue(true);
		} catch (ASServicesException | MyException e) {
			fail(e.getMessage());
		}
			
	}

	/**
	 * Test method for {@link udea.services.RespuestaPeticionServicio#getRespuestas()}.
	 */
	//@Test
	public void testGetRespuestas() {
		List<RespuestaPeticionDTO> lista;
		try {
			lista = respuestaPeticionServicio.getRespuestas();
			assertTrue(true);
		} catch (MyException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for {@link udea.services.RespuestaPeticionServicio#getRespuesta(java.lang.Integer)}.
	 */
	//@Test
	public void testGetRespuesta() {
		RespuestaPeticionDTO res;
		try {
			res = respuestaPeticionServicio.getRespuesta(1);
			assertTrue(true);
		} catch (MyException e) {
			fail(e.getMessage());
		}
	}

}
