/**
 * Prueba unitaria para UsuarioImp
 * ver udea.dao.implement.UsuarioImp
 */

package udea.test;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import udea.dao.implement.UsuarioImp;
import udea.dto.UsuarioDTO;
import udea.exception.MyException;

@RunWith(SpringJUnit4ClassRunner.class)
@Component
@ContextConfiguration(locations = "classpath:springconfig.xml")
/**
 * @author Stephany Berrio Alzate
 *
 */
public class UsuarioImpTest {

	/**
	 * Test method for
	 * {@link udea.dao.implement.UsuarioImp#insert(udea.dto.UsuarioDTO)}.
	 */
	@Autowired
	UsuarioImp prod;

	@Test
	public void testInsert() {

		try {
			Random rnd = new Random();
			int num = (int) (rnd.nextDouble() * 100);
			UsuarioDTO p = new UsuarioDTO();
			p.setNombre("algo");
			p.setId(num);
			p.setContrasena("algo");
			p.setUsuario("algo");
			p.setEmail("algo");
			p.setTipoUsuario("administrador");
			p.setTelefono("5453235");			
			prod.insert(p);
			assertTrue(true);

		} catch (Exception e) {
			fail(e.getMessage());

		}
	}

	/**
	 * Test method for
	 * {@link udea.dao.implement.UsuarioImp#update(udea.dto.UsuarioDTO)}.
	 */
	@Test
	public void testUpdate() {
		try {
			UsuarioDTO p = new UsuarioDTO();
			p.setNombre("actualice");
			p.setId(2);
			p.setNombre("algo");
			
			p.setContrasena("algo");
			p.setUsuario("algo");
			p.setEmail("algo");
			p.setTipoUsuario("administrador");
			p.setTelefono("5453235");	
			prod.update(p);
			assertTrue(true);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link udea.dao.implement.UsuarioImp#remove(udea.dto.UsuarioDTO)}.
	 */
	@Test
	public void testRemove() {
		try {
			UsuarioDTO p = new UsuarioDTO();
			
			p.setId(3);
			prod.remove(p);
			assertTrue(true);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for {@link udea.dao.implement.UsuarioImp#getAll()}.
	 */
	@Test
	public void testGetAll() {

		try {
			List<UsuarioDTO> usuarios = prod.getAll();
			for (UsuarioDTO d : usuarios) {
				System.out.println(d.getNombre());
			}

			assertTrue(true);

		} catch (MyException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link udea.dao.implement.UsuarioImp#get(java.lang.Integer)}.
	 */
	@Test
	public void testGet() {
		try {
			UsuarioDTO usuario = prod.get("25");
			System.out.println("nombre="+usuario.getNombre());
			assertTrue(true);

		} catch (MyException e) {
			fail(e.getMessage());
		}
	}

}
