/**
 * Prueba unitaria para SucursalImp
 * ver udea.dao.implement.SucursalImp
 */
package udea.test;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import udea.dao.implement.SucursalImp;
import udea.dto.SucursalDTO;
import udea.exception.MyException;

@RunWith(SpringJUnit4ClassRunner.class)
@Component
@ContextConfiguration(locations = "classpath:springconfig.xml")
/**
 * @author Stephany Berrio Alzate
 *
 */
public class SucursalImpTest {

	/**
	 * Test method for
	 * {@link udea.dao.implement.SucursalImp#insert(udea.dto.SucursalDTO)}.
	 */
	@Autowired
	SucursalImp prod;

	@Test
	public void testInsert() {

		try {
			Random rnd = new Random();
			int num = (int) (rnd.nextDouble() * 100);
			SucursalDTO p = new SucursalDTO();
			p.setNombre("algo");
			p.setId(num);
			prod.insert(p);
			assertTrue(true);

		} catch (Exception e) {
			fail(e.getMessage());

		}
	}

	/**
	 * Test method for
	 * {@link udea.dao.implement.SucursalImp#update(udea.dto.SucursalDTO)}.
	 */
	@Test
	public void testUpdate() {
		try {
			SucursalDTO p = new SucursalDTO();
			p.setNombre("actualice");
			p.setId(1);
			prod.update(p);
			assertTrue(true);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link udea.dao.implement.SucursalImp#remove(udea.dto.SucursalDTO)}.
	 */
	@Test
	public void testRemove() {
		try {
			SucursalDTO p = new SucursalDTO();
			p.setNombre("algo");
			p.setId(3);
			prod.remove(p);
			assertTrue(true);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for {@link udea.dao.implement.SucursalImp#getAll()}.
	 */
	@Test
	public void testGetAll() {

		try {
			List<SucursalDTO> sucursales = prod.getAll();
			for (SucursalDTO d : sucursales) {
				System.out.println(d.getNombre());
			}

			assertTrue(true);

		} catch (MyException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link udea.dao.implement.SucursalImp#get(java.lang.Integer)}.
	 */
	@Test
	public void testGet() {
		try {
			SucursalDTO sucursal = prod.get(1);
			System.out.println(sucursal.getNombre());
			assertTrue(true);

		} catch (MyException e) {
			fail(e.getMessage());
		}
	}

}
