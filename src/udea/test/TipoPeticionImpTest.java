/**
 * Prueba unitaria para TipoPeticionImp
 * ver udea.dao.implement.TipoPeticionImp
 */
package udea.test;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import udea.dao.implement.TipoPeticionImp;
import udea.dto.TipoPeticionDTO;
import udea.exception.MyException;

@RunWith(SpringJUnit4ClassRunner.class)
@Component
@ContextConfiguration(locations = "classpath:springconfig.xml")
/**
 * @author Stephany Berrio Alzate
 *
 */
public class TipoPeticionImpTest {

	/**
	 * Test method for
	 * {@link udea.dao.implement.TipoPeticionImp#insert(udea.dto.TipoPeticionDTO)}.
	 */
	@Autowired
	TipoPeticionImp prod;

	@Test
	public void testInsert() {

		try {
			Random rnd = new Random();
			int num = (int) (rnd.nextDouble() * 100);
			TipoPeticionDTO p = new TipoPeticionDTO();
			p.setNombre("algo");
			p.setId(num);
			prod.insert(p);
			assertTrue(true);

		} catch (Exception e) {
			fail(e.getMessage());

		}
	}

	/**
	 * Test method for
	 * {@link udea.dao.implement.TipoPeticionImp#update(udea.dto.TipoPeticionDTO)}.
	 */
	@Test
	public void testUpdate() {
		try {
			TipoPeticionDTO p = new TipoPeticionDTO();
			p.setNombre("actualice");
			p.setId(1);
			prod.update(p);
			assertTrue(true);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link udea.dao.implement.TipoPeticionImp#remove(udea.dto.TipoPeticionDTO)}.
	 */
	@Test
	public void testRemove() {
		try {
			TipoPeticionDTO p = new TipoPeticionDTO();
			p.setNombre("algo");
			p.setId(3);
			prod.remove(p);
			assertTrue(true);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for {@link udea.dao.implement.TipoPeticionImp#getAll()}.
	 */
	@Test
	public void testGetAll() {

		try {
			List<TipoPeticionDTO> tipos = prod.getAll();
			for (TipoPeticionDTO d : tipos) {
				System.out.println(d.getNombre());
			}

			assertTrue(true);

		} catch (MyException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link udea.dao.implement.TipoPeticionImp#get(java.lang.Integer)}.
	 */
	@Test
	public void testGet() {
		try {
			TipoPeticionDTO tipo = prod.get(1);
			System.out.println(tipo.getNombre());
			assertTrue(true);

		} catch (MyException e) {
			fail(e.getMessage());
		}
	}

}
