/**
 * 
 */
package udea.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import udea.dto.UsuarioDTO;
import udea.exception.ASServicesException;
import udea.exception.MyException;
import udea.services.UsuarioServicio;

/**
 * @author Stephany Berrio Alzate
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Component
@ContextConfiguration(locations = "classpath:springconfig.xml")
public class UsuarioServicioTest {
	@Autowired
	UsuarioServicio usuarioServicio;
	/**
	 * Test method for {@link udea.services.UsuarioServicio#insertUsuario(java.lang.Integer, java.lang.String, java.lang.Integer, java.lang.Integer)}.
	 */
	@Test
	public void testLogIn() {
		try {
			usuarioServicio.logIn("1", "1");
			assertTrue(true);
		} catch (ASServicesException | MyException e) {
			fail(e.getMessage());
		}
			
	}

	/**
	 * Test method for {@link udea.services.UsuarioServicio#getUsuarios(java.lang.Integer)}.
	 */
	//@Test
//	public void testGetUsuario() {
//		UsuarioDTO res;
//		try {
//			res = usuarioServicio.getUsuario("1");
//			assertTrue(true);
//		} catch (MyException e) {
//			fail(e.getMessage());
//		}
//	}

	/**
	 * Test method for {@link udea.services.UsuarioServicio#getUsuario(java.lang.Integer)}.
	 */
	@Test
	public void testGetUsuarios() {
		List<UsuarioDTO> lista;
		try {
			lista = usuarioServicio.getUsuarios();
			assertTrue(true);
		} catch (MyException e) {
			fail(e.getMessage());
		}
	}

}
