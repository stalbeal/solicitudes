/**
 * 
 */
package udea.test;

import static org.junit.Assert.*;

import java.rmi.RemoteException;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import udea.dto.PeticionDTO;
import udea.dto.ProductoDTO;
import udea.exception.ASServicesException;
import udea.exception.MyException;
import udea.services.PeticionServicio;

/**
 * @author Stephany Berrio Alzate
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Component
@ContextConfiguration(locations = "classpath:springconfig.xml")
public class PeticionServicioTest {
	@Autowired
	PeticionServicio peticionServicio;

	/**
	 * Test method for {@link udea.services.PeticionServicio#getPeticiones()}.
	 */
	@Test
	public void testGetPeticiones() {
		List<PeticionDTO> lista;
		try {
			lista = peticionServicio.getPeticiones();
			assertTrue(true);
		} catch (ASServicesException e) {
			e.printStackTrace();
		} catch (MyException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Test method for
	 * {@link udea.services.PeticionServicio#insertPeticion(java.lang.Integer, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer, java.lang.Integer)}
	 * .
	 * 
	 * @throws RemoteException
	 */
	@Test
	public void testInsertPeticion() throws RemoteException {
		try {

			peticionServicio.insertPeticion(100, "nombres", "apellidos",
					"stalbeal10@gmail.com", "3333", "3333", "descripcion", 1,
					1, 1);
			assertTrue(true);
		} catch (Exception e) {

			throw new RemoteException(e.getMessage());
		}

	}

	/**
	 * Test method for
	 * {@link udea.services.PeticionServicio#getPeticion(java.lang.Integer)}.
	 */
	@Test
	public void testGetPeticion() {
		PeticionDTO peticion;
		try {
			peticion = peticionServicio.getPeticion(1);
			assertTrue(true);
		} catch (MyException e) {
			e.printStackTrace();
		}
	}

}
