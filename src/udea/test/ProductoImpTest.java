/**
 * Prueba unitaria para ProductoImp
 * ver udea.dao.implement.ProductoImp
 */
package udea.test;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import udea.dao.implement.ProductoImp;
import udea.dto.ProductoDTO;
import udea.exception.MyException;

@RunWith(SpringJUnit4ClassRunner.class)
@Component
@ContextConfiguration(locations = "classpath:springconfig.xml")
/**
 * @author Stephany Berrio Alzate
 *
 */
public class ProductoImpTest {

	/**
	 * Test method for
	 * {@link udea.dao.implement.ProductoImp#insert(udea.dto.ProductoDTO)}.
	 */
	@Autowired
	ProductoImp prod;

	@Test
	public void testInsert() {

		try {
			Random rnd = new Random();
			int num = (int) (rnd.nextDouble() * 100);
			ProductoDTO p = new ProductoDTO();
			p.setNombre("algo");
			p.setId(num);
			prod.insert(p);
			assertTrue(true);

		} catch (Exception e) {
			fail(e.getMessage());

		}
	}

	/**
	 * Test method for
	 * {@link udea.dao.implement.ProductoImp#update(udea.dto.ProductoDTO)}.
	 */
	@Test
	public void testUpdate() {
		try {
			ProductoDTO p = new ProductoDTO();
			p.setNombre("actualice");
			p.setId(1);
			prod.update(p);
			assertTrue(true);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link udea.dao.implement.ProductoImp#remove(udea.dto.ProductoDTO)}.
	 */
	@Test
	public void testRemove() {
		try {
			ProductoDTO p = new ProductoDTO();
			p.setNombre("algo");
			p.setId(3);
			prod.remove(p);
			assertTrue(true);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for {@link udea.dao.implement.ProductoImp#getAll()}.
	 */
	@Test
	public void testGetAll() {

		try {
			List<ProductoDTO> products = prod.getAll();
			for (ProductoDTO d : products) {
				System.out.println(d.getNombre());
			}

			assertTrue(true);

		} catch (MyException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link udea.dao.implement.ProductoImp#get(java.lang.Integer)}.
	 */
	@Test
	public void testGet() {
		try {
			ProductoDTO product = prod.get(1);
			System.out.println(product.getNombre());
			assertTrue(true);

		} catch (MyException e) {
			fail(e.getMessage());
		}
	}

}
