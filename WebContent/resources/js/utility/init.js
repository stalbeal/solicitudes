(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('.parallax').parallax();
    $('select').material_select();
    $(".button-collapse").sideNav();
    $('.modal-trigger').leanModal();

  }); // end of document ready
})(jQuery); // end of jQuery name space