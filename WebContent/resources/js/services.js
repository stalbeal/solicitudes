angular.module('solicitudesApp.services', [])

//Factory para consumir el servicio de login
.factory('loginServise',function($http) {
			var login = {};
			login.getLogin = function(username, password) {
				return $http({
					method : 'GET',
					url : '../rest/usuarios/login?username=' + username
							+ '&password=' + password
				});
			}
			return login;
		})
//Factory para consumir los servicios de peticiones
.factory('peticionService',function($http) {

			var peticiones = {};
			//obtener las peticiones
			peticiones.getPeticiones = function() {
				return $http({
					method : 'GET',
					url : '../rest/peticiones/'
				});
			}
			//crear una nueva peticion
			peticiones.crearSolicitud = function(id, nombres, apellidos, email,
					telefono, celular, descripcion, tipoPeticionId, productoId,
					sucursalId) {
				return $http({
					method : 'POST',
					url : '../rest/peticiones/create?id=' + id + '&nombres='
							+ nombres + '&apellidos=' + apellidos + '&email='
							+ email + '&telefono=' + telefono + '&celular='
							+ celular + '&descripcion=' + descripcion
							+ '&tipoPeticionId=' + tipoPeticionId
							+ '&productoId=' + productoId + '&sucursalId='
							+ sucursalId
				});
			}

			return peticiones;
		})
//Factory para consumir los servicios de sucursales
.factory('sucursalService', function($http) {

	var sucursales = {};
	//obtener las sucursales
	sucursales.getSucursales = function() {
		return $http({
			method : 'GET',
			url : '../rest/sucursales/'
		});
	}

	return sucursales;
})
//Factory para consumir los servicios de productos
.factory('productoService', function($http) {

	var productos = {};
	//obtener todos los productos
	productos.getProductos = function() {
		return $http({
			method : 'GET',
			url : '../rest/productos/'
		});
	}

	return productos;
})
//Factory para consumir los servicios de tipoPeticiones
.factory('tipoPeticionService', function($http) {

	var tiposPeticion = {};
	//obtener todos los tipos de peticiones
	tiposPeticion.getTiposPeticion = function() {
		return $http({
			method : 'GET',
			url : '../rest/tipopeticiones/'
		});
	}

	return tiposPeticion;
});