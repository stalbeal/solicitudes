/*
 * Módulo para los controladores
 */
angular
    .module('solicitudesApp.controllers', [])

/* 
 * Controlador para el Login
 */
.controller(
    'loginCtrl',
    function($scope, $cookies, $location, $rootScope, loginServise) {

        $scope.user = {};
        // console.log('cookies');
        // console.log($cookies);
        $scope.login = function() {
        	//Se validadn los datos ingresados
            if ($scope.user.password == "" && $scope.user.password == undefined && $scope.user.username == "" && $scope.user.username == undefined) {
                alert("Por favor ingrese el nombre de usuario y la contraseña");
            } else if ($scope.user.username == "" || $scope.user.username == undefined) {
                alert("Por favor ingrese el nombre de usuario");
            } else if ($scope.user.password == "" || $scope.user.password == undefined) {
                alert("Por favor ingrese la contraseña");
            }
            /*
             * Se consume el servicio de login
             * @param username -Nombre de Usuario
             * @param password -Contraseña
             * @return response -JSON con la información del usuario logueado
             */
            loginServise.getLogin($scope.user.username,
                $scope.user.password).success(
                function(response) {
                    //Se le asigna al scope
                    $scope.loggedUser = response;
                    console.log($scope.loggedUser);
                    if ($scope.loggedUser) {
                        //redirección a /solicitudes
                    	$rootScope.loggedUser=response;
                        $location.path('/solicitudes');
                    }else{
                    	alert('Lo sentimos la información que ingresaste no es correcta.');
                    }
                });

        }

    })

/* 
 * Controlador de index de las solicitudes
 * @return JSON con todas las peticiones existentes en la base de datos
 */
.controller('solicitudesCtrl',
    function($scope, $rootScope,$location, peticionService) {
        //Se consume el servicio de peticiones
	
		if($rootScope.loggedUser){
        peticionService.getPeticiones().success(function(response) {
            $scope.peticiones = response;
            console.log($scope.peticiones);

        });
		}else{
			 $location.path('/login');
		}
    })

/* 
 * Controlador para crear solicitudes
 */
.controller(
    'solicitudNuevaCtrl',
    function($scope, $routeParams, productoService,
        sucursalService, tipoPeticionService, $location, peticionService) {
        //Se consume el servicio para obtener los productos existentes en la base de datos
        productoService.getProductos().success(function(productos) {
            //Se asignan los productos obtenidos al scope
            $scope.productos = productos.productoDTO;
            console.log(productos.productoDTO);
            //Se consume el servicio para obtener todas las sucursales existentes en la 
            //base de datos
            sucursalService.getSucursales().success(function(sucursales) {
                //Se asigna al scope
                $scope.sucursales = sucursales.sucursalDTO;
                console.log(sucursales.sucursalDTO);
                //Se consume el servicio para obtener los tipos de peticiones 
                //existentes en la base de datos
                tipoPeticionService.getTiposPeticion().success(function(tiposPeticion) {
                    // se asigna al scope
                    $scope.tiposPeticion = tiposPeticion.tipoPeticionDTO;
                    console.log(tiposPeticion.tipoPeticionDTO);
                });
            });

        });

        $scope.crearSolicitud = function() {
        	//Se valida que todos los campos esten llenos
            if ($scope.tipoPeticion == null || $scope.name == null || $scope.lastName == null || $scope.phoneNumber == null || $scope.email == null || $scope.sucursal == null || $scope.producto == null) {
                alert('Por favor diligencie los datos requeridos');
                return;
            }

            //Se reciben las vbles obtenidas en el formulario 
            //de nueva solicitud
            var nombres = $scope.name;
            var apellidos = $scope.lastName;
            var celular = $scope.mobile;
            var telefono = $scope.phoneNumber;
            var descripcion = $scope.description;
            var email = $scope.email;
            var estadoPeticion = "false";
            var fechaSolicitud = new Date();
            var id = Math.floor((Math.random() * 10) + 1000);
            var productoId = $scope.producto;
            var sucursalId = $scope.sucursal;
            var tipoPeticionId = $scope.tipoPeticion;
            // se consume el servicio para crear una nueva solicitud
            peticionService.crearSolicitud(id, nombres, apellidos,
                email, telefono, celular, descripcion,
                tipoPeticionId, productoId, sucursalId)
                .success(function(response) {
                    //En caso de que la respuesta sea exitosa se redirecciona a una nueva ventana
                    if (response == 'Exito') {
                        // $cookies.put('userId',$scope.loggedUser.id);
                        $location.path('/registroexitoso');
                    }else{
                    	alert('No se ha podido crear la solicitud, por favor verifique los datos.');
                    }

                });
        }

    });