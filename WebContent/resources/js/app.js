
/*
 * Módulo principal  
 */
angular.module(
		'solicitudesApp',
		[ 'solicitudesApp.services', 'solicitudesApp.controllers', 'ngRoute',
				'ngCookies' ])
.config(
		[ '$routeProvider', function($routeProvider) {
			//Configuración de las rutas
			$routeProvider.when("/login", {
				templateUrl : "views/login.html",
				controller : "loginCtrl"
			}).when("/solicitudes", {
				templateUrl : "views/solicitudes.html",
				controller : "solicitudesCtrl"
			}).when("/nuevasolicitud", {
				templateUrl : "views/solicitud.html",
				controller : "solicitudNuevaCtrl"
			}).when("/registroexitoso", {
				templateUrl : "views/registroexitoso.html"
			}).otherwise({
				redirectTo : '/login'
			});
		} ])
/*
 * Filtro para mostrar los estados de las solicitudes según sea el caso
 * true=Solucionado
 * false=Pendiente
 */
.filter('changeStatus', function() {
	return function(input) {
		return input ? 'Solucionada' : 'Pendiente';
	}
});